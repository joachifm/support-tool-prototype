{-
   Support tool API
   Support tools

   The version of the OpenAPI document: 0.1

   NOTE: This file is auto generated by the openapi-generator.
   https://github.com/openapitools/openapi-generator.git
   Do not edit this file manually.
-}


module Data.ReportModel exposing (ReportModel, decoder, encode, encodeWithTag, toString)

import Data.ReportItemModel as ReportItemModel exposing (ReportItemModel)
import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (optional, required)
import Json.Encode as Encode


type alias ReportModel =
    { runid : String
    , secondsSinceEpoch : Int
    , imei : String
    , items : (Dict String ReportItemModel)
    }


decoder : Decoder ReportModel
decoder =
    Decode.succeed ReportModel
        |> required "runid" Decode.string
        |> required "secondsSinceEpoch" Decode.int
        |> required "imei" Decode.string
        |> required "items" (Decode.dict ReportItemModel.decoder)



encode : ReportModel -> Encode.Value
encode =
    Encode.object << encodePairs


encodeWithTag : ( String, String ) -> ReportModel -> Encode.Value
encodeWithTag (tagField, tag) model =
    Encode.object <| encodePairs model ++ [ ( tagField, Encode.string tag ) ]


encodePairs : ReportModel -> List (String, Encode.Value)
encodePairs model =
    [ ( "runid", Encode.string model.runid )
    , ( "secondsSinceEpoch", Encode.int model.secondsSinceEpoch )
    , ( "imei", Encode.string model.imei )
    , ( "items", (Encode.dict identity ReportItemModel.encode) model.items )
    ]



toString : ReportModel -> String
toString =
    Encode.encode 0 << encode




