# Overview

- `example` example data, organized according to endpoint structure
- `lib` generated API clients
- `spec` versioned openapi specifications
