{ nixpkgsSrc ? <nixpkgs>
, nixpkgs ? import nixpkgsSrc {}
, lib ? import (nixpkgsSrc + "/lib")
}:

let
  inherit (nixpkgs) pkgs;
in

rec {

  elmDevShell = pkgs.mkShell {
    nativeBuildInputs = with pkgs; [
      elmPackages.elm
      elmPackages.elm-format
    ];
  };

}
