module Main exposing (..)

import Browser exposing (UrlRequest)
import Data.ReportItemModel as ReportItemModel exposing (ReportItemModel)
import Data.ReportModel as ReportModel exposing (ReportModel)
import Dict exposing (Dict)
import Html as H exposing (Html)
import Html.Attributes as A
import Html.Events as E
import Html.Keyed as Keyed
import Html.Lazy exposing (lazy)
import Http
import Maybe
import Regex
import Request.Default exposing (getReportJson)
import Url exposing (Url)
import Url.Parser
import Url.Parser.Query


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = onUrlRequest
        , onUrlChange = onUrlChange
        }


type Msg
    = NoOp
      -- Input IMEI has been changed.
    | SetInputImei String
      -- Submit form to initiate device self-check.
    | SubmitForm
      -- Got report result.
    | GotReport (Result Http.Error ReportModel)
      -- Got deferred item; replaces existing item in current report.
    | GotReportItem (Result Http.Error ReportItemModel)
      -- Get deferred item
    | GetReportItem String


type alias Flags =
    ()


type alias Model =
    { inputError : String
    , inputImei : String
    , fetchError : String
    , isLoading : Bool

    -- Loaded report data
    , loadedReport : Maybe ReportModel
    }


init : Flags -> Url -> key -> ( Model, Cmd Msg )
init _ navUrl _ =
    let
        model =
            { inputError = ""
            , inputImei = Maybe.withDefault "" (parseQueryParam "imei" navUrl)
            , fetchError = ""
            , isLoading = False
            , loadedReport = Nothing
            }
    in
    if model.inputImei /= "" then
        update SubmitForm model

    else
        ( model, Cmd.none )


parseQueryParam : String -> Url -> Maybe String
parseQueryParam q url =
    case Url.Parser.parse (Url.Parser.query (Url.Parser.Query.string q)) url of
        Just inner ->
            inner

        Nothing ->
            Nothing


onUrlRequest : UrlRequest -> Msg
onUrlRequest _ =
    NoOp


onUrlChange : Url -> Msg
onUrlChange _ =
    NoOp


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SetInputImei s ->
            if isValidImei s then
                ( { model
                    | inputImei = s
                    , inputError = ""
                  }
                , Cmd.none
                )

            else
                ( { model
                    | inputImei = s
                    , inputError = "IMEI is a 15-digit string"
                  }
                , Cmd.none
                )

        GetReportItem url ->
            ( { model
                | fetchError = ""
                , isLoading = True
              }
            , fetchReportItem url
            )

        SubmitForm ->
            ( { model
                | fetchError = ""
                , isLoading = True

                -- Retain existing report data until new report is loaded successfully
              }
            , getReportJson { onSend = GotReport }
            )

        GotReportItem result ->
            case result of
                Ok resultData ->
                    ( { model
                        | fetchError = ""
                        , isLoading = False
                        , loadedReport =
                            Maybe.map (replaceReportItem resultData)
                                model.loadedReport
                      }
                    , Cmd.none
                    )

                Err httpError ->
                    ( { model
                        | fetchError = httpErrorToString httpError
                      }
                    , Cmd.none
                    )

        GotReport result ->
            case result of
                Ok resultData ->
                    ( { model
                        | fetchError = ""
                        , isLoading = False
                        , loadedReport = Just resultData
                      }
                    , Cmd.none
                    )

                Err httpError ->
                    ( { model
                        | fetchError = httpErrorToString httpError
                        , isLoading = False

                        -- Retain existing report data
                      }
                    , Cmd.none
                    )


httpErrorToString : Http.Error -> String
httpErrorToString httpError =
    case httpError of
        Http.BadUrl s ->
            "Unable to reach URL " ++ s

        Http.Timeout ->
            "Request timed out"

        Http.NetworkError ->
            "Network error"

        Http.BadStatus code ->
            "Unexpected HTTP status code " ++ String.fromInt code

        Http.BadBody body ->
            "Malformed response body: " ++ body


replaceReportItem : ReportItemModel -> ReportModel -> ReportModel
replaceReportItem item report =
    case item.id of
        Just theid ->
            { report
                | items =
                    Dict.map
                        (\k v ->
                            if k == theid then
                                item

                            else
                                v
                        )
                        report.items
            }

        Nothing ->
            report


view : Model -> Browser.Document Msg
view model =
    { title = "Get device report"
    , body = viewMain model
    }


viewMain : Model -> List (Html Msg)
viewMain model =
    [ H.div []
        [ lazy viewInputControls model
        , lazy viewLoadStatus model
        , lazy viewReport model
        ]
    ]


viewLoadStatus : Model -> Html Msg
viewLoadStatus model =
    if model.isLoading then
        H.div [] [ H.text "Loading ..." ]

    else if model.fetchError /= "" then
        H.div [] [ H.text ("Fetch error: " ++ model.fetchError) ]

    else
        H.div [] []


viewReport : Model -> Html Msg
viewReport model =
    case model.loadedReport of
        Nothing ->
            H.div [] []

        Just report ->
            H.div []
                [ viewReportHeading report
                , viewReportItems (Dict.toList report.items)
                ]


viewReportHeading : ReportModel -> Html Msg
viewReportHeading report =
    H.div []
        [ H.div [] [ H.text ("IMEI: " ++ report.imei) ]
        , H.div [] [ H.text ("Time: " ++ String.fromInt report.secondsSinceEpoch) ]
        , H.div [] [ H.text ("Run id: " ++ report.runid) ]
        ]


viewReportItems : List ( String, ReportItemModel ) -> Html Msg
viewReportItems items =
    Keyed.node "div" [] (List.map viewReportItemKeyed items)


viewReportItemKeyed : ( String, ReportItemModel ) -> (String, Html Msg)
viewReportItemKeyed ( itemId, item ) = ( itemId, viewReportItem ( itemId, item ) )


viewReportItem : ( String, ReportItemModel ) -> Html Msg
viewReportItem ( itemId, item ) =
    H.div [ A.id itemId ]
        [ H.div [] [ H.text item.title ]
        , H.div []
            [ H.div []
                (Maybe.withDefault []
                    (Maybe.map (\x -> [ H.text ("Description: " ++ x) ])
                        item.description
                    )
                )
            ]
        , H.div []
            [ H.div []
                (Maybe.withDefault []
                    (Maybe.map (\x -> [ H.text ("Error: " ++ x) ])
                        item.error
                    )
                )
            , H.div []
                (Maybe.withDefault []
                    (Maybe.map
                        (\x ->
                            [ H.text
                                (if x then
                                    "PASS"

                                 else
                                    "FAIL"
                                )
                            ]
                        )
                        item.passFail
                    )
                )
            , H.div []
                (Maybe.withDefault []
                    (Maybe.map
                        (\url ->
                            [ H.button
                                [ E.onClick (GetReportItem url) ]
                                [ H.text "Get deferred result" ]
                            ]
                        )
                        item.jobUrl
                    )
                )
            , H.div []
                (Maybe.withDefault []
                    (Maybe.map
                        (\url ->
                            [ H.button
                                [ E.onClick (GetReportItem url) ]
                                [ H.text "Trigger job" ]
                            ]
                        )
                        item.triggerUrl
                    )
                )
            ]
        , H.div []
            [ H.div []
                (Maybe.withDefault []
                    (Maybe.map (\txt -> [ H.text txt ]) item.detailText)
                )
            , H.div []
                [ H.ul []
                    (Maybe.withDefault []
                        (Maybe.map (List.map (\i -> H.li [] [ H.text i ]))
                            item.detailList
                        )
                    )
                ]
            , H.div []
                [ H.ul []
                    (Maybe.withDefault []
                        (Maybe.map
                            (\m ->
                                List.map
                                    (\( k, v ) ->
                                        H.li []
                                            [ H.text (k ++ ": " ++ v)
                                            ]
                                    )
                                    (Dict.toList m)
                            )
                            item.detailMap
                        )
                    )
                ]
            ]
        ]


viewInputControls model =
    H.div []
        [ H.input
            [ A.type_ "text"
            , A.placeholder "123456789012345"
            , A.value model.inputImei
            , A.pattern imeiRegexPattern
            , A.minlength 15
            , A.maxlength 15
            , A.required True
            , A.disabled model.isLoading
            , E.onInput SetInputImei
            ]
            []
        , H.button
            [ A.disabled (not (isValidImei model.inputImei) || model.isLoading)
            , E.onClick SubmitForm
            ]
            [ H.text "Get report" ]
        ]


viewInputValidation : Model -> Html Msg
viewInputValidation model =
    if model.inputError /= "" then
        H.div [ A.style "color" "red" ] [ H.text model.inputError ]

    else
        H.div [ A.style "color" "green" ] [ H.text "OK" ]


isValidImei : String -> Bool
isValidImei s =
    Regex.contains imeiRegex s


isPartialImei : String -> Bool
isPartialImei s =
    String.length s < 15 && String.all Char.isDigit s


imeiRegex : Regex.Regex
imeiRegex =
    Maybe.withDefault Regex.never <|
        Regex.fromString imeiRegexPattern


imeiRegexPattern : String
imeiRegexPattern =
    "^[1-9]\\d{14}$"


fetchReportItem : String -> Cmd Msg
fetchReportItem jobUrl =
    Http.get
        { url = jobUrl
        , expect = Http.expectJson GotReportItem ReportItemModel.decoder
        }
