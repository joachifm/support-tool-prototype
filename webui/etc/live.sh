#! /bin/sh -eu
build_state=
while : ; do
    if ! elm make --debug src/Main.elm --output=app.js ; then
        build_state="broken"
    else
        build_state="ok"
    fi
    echo "State: $build_state"
    inotifywait src/Main.elm elm.json -e modify
done
