#! /bin/sh

exec docker run --rm \
    -u "$(id -u):$(id -u)" \
    -v "$HOME/.npm:/home/node/.npm:rw" \
    -v "$PWD:/home/node/src:rw" \
    -w /home/node/src \
    --net host \
    -e NODE_PATH=/home/node/src/node_modules \
    -e ELM_HOME=/home/node/src/_elm \
    -e PATH=/home/node/src/node_modules/.bin:/usr/local/bin:/usr/bin:/bin \
    -it \
    --entrypoint /bin/bash \
    node:lts \
    "$@"
