#! /bin/sh

# Adapted from https://guide.elm-lang.org/optimization/asset_size.html

js=app.js
min=app.min.js
complevel=6  # determined experimentally that levels above 6 do not improve size

set -eu
elm make --optimize src/Main.elm --output="$js"
uglifyjs "$js" --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output "$min"

echo "Compiled size : $(wc -c $js)"
echo "Minified size : $(wc -c $min)"
echo "Gzipped size  : $(gzip -$complevel -c $min | wc -c)"
