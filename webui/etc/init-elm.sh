#! /bin/sh

# Non-interactively initialize Elm stuff

yes | elm init
while read -r dep ; do
    yes | elm install "$dep"
done <<EOF
NoRedInk/elm-json-decode-pipeline
elm/http
elm/json
elm/regex
elm/url
EOF
